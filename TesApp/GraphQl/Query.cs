﻿using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using HotChocolate.Types.Pagination;
using System;
using System.Linq;
using System.Threading.Tasks;
using TesApp.Data;
using TesApp.Services;

namespace TesApp
{
    public class Query
    {
        [UseDbContext(typeof(VenuesDBContext))]
        [GraphQLDescription("Retrieving venues by category with paging")]
        [UsePaging()]
        [UseProjection()]
        public async Task<Connection<IVenueModel>> Venues(
            IResolverContext context,
            [GraphQLNonNullType] string category,
            [GraphQLNonNullType] int first,
            string after,
            string before)
        {
            long afterId;
            long beforeId;
            Int64.TryParse(after, out long parsedAfter);
                     afterId = parsedAfter;
            Int64.TryParse(before, out long parsedBefore);
                     beforeId = parsedBefore;

            var venues = await context
                .Service<IVenuesCachedService>()
                    .GetVenuesByCategoryAsync(
                        context,
                        category,
                        afterId,
                        beforeId,
                        first,
                        context.RequestAborted);

            var edges = venues
                           .Select(venue => new Edge<IVenueModel>(venue as IVenueModel, venue.Id.ToString()))
                           .ToList();

            var pageInfo = new ConnectionPageInfo(false, false, null, null);

            var connection = new Connection<IVenueModel>(edges, pageInfo,
                                cancelationTokern => new ValueTask<int>(0));

            return connection;
        }

        [UseDbContext(typeof(VenuesDBContext))]
        [UseProjection]
        public Task<ICategory[]> Categories(
              IResolverContext context)
        {
            return context
                .Service<IVenuesCachedService>()
                    .GetCategories(context.RequestAborted);
        }
    }
}
