﻿using System.Linq;
using System.Linq.Expressions;
using System;

namespace TesApp.ExpressionUtils
{
    public interface IQueryBuilder
    {
        Expression<Func<T, bool>> And<T>(Expression<Func<T, bool>> first, Expression<Func<T, bool>> second);
        Expression<Func<T, bool>> Create<T>(Expression<Func<T, bool>> predicate);
        Expression<Func<T, bool>> False<T>();
        Expression<Func<T, bool>> Not<T>(Expression<Func<T, bool>> expression);
        Expression<Func<T, bool>> Or<T>(Expression<Func<T, bool>> first, Expression<Func<T, bool>> second);
        Expression<Func<T, bool>> True<T>();
        
        IQueryable<T> OrderByDynamic<T, TKey>(Expression<Func<T, TKey>> member, bool descending, bool none, IQueryable<T> query);
    }
}