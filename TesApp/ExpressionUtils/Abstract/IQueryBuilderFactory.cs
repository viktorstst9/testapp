﻿namespace TesApp.ExpressionUtils
{
    public interface IQueryBuilderFactory
    {
        IQueryBuilder Create();
    }
}