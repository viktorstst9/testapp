﻿namespace TesApp.ExpressionUtils
{
    public class QueryBuilderFactory : IQueryBuilderFactory
    {
        public IQueryBuilder Create() => new QueryBuilder();
    }
}
