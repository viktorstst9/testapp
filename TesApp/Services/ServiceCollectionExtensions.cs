﻿using Flurl.Http.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TesApp.Data;
using TesApp.ExpressionUtils;
using TesApp.Services;

namespace TesApp
{
    public static class ServiceCollectionExtensions
    {
        private static class Constants
        {
            public const string DefaultConnectionConfigKey = "DefaultConnection";
            public const string DefaultVenuesEndpointConfigKey = "Endpoints:VenuesBaseUrl";
        }

        public static IServiceCollection AddModels(this IServiceCollection services)
        {
                services
                  .AddSingleton<IModelBaseFactory, ModelBaseFactory>(serviceProvider => new ModelBaseFactory(serviceProvider));

                services
                  .AddSingleton<IVenueModelFactory, VenueModelFactory>(serviceProvider => new VenueModelFactory());

            return services;
        }

        public static IServiceCollection AddDBServices(
            this IServiceCollection services,
             IConfiguration configuration)
        {
            services.AddDbContextFactory<VenuesDBContext>(
              options => options.UseSqlServer(configuration.GetConnectionString(Constants.DefaultConnectionConfigKey)));

            services
                .AddScoped<ICategoriesDBService, CategoriesDBService>(_ => new CategoriesDBService(_.GetRequiredService<IDbContextFactory<VenuesDBContext>>()));

            services
                .AddScoped<IVenuesDBService, VenuesDBService>(_ => new VenuesDBService(_.GetRequiredService<IDbContextFactory<VenuesDBContext>>(),
                   _.GetRequiredService<IQueryBuilderFactory>(),
                   _.GetRequiredService<IModelBaseFactory>()));

            services
              .AddScoped<IVenuesDBBatchAddService, VenuesDBAddService>(_ => new VenuesDBAddService(_.GetRequiredService<IDbContextFactory<VenuesDBContext>>()));

            services
              .AddScoped<IVenuesDBService, VenuesDBService>(_ => new VenuesDBService(_.GetRequiredService<IDbContextFactory<VenuesDBContext>>(),
                 _.GetRequiredService<IQueryBuilderFactory>(),
                 _.GetRequiredService<IModelBaseFactory>()));

            services
              .AddScoped<IVenuesCachedService, VenuesCaschedService>(_ =>
                  new VenuesCaschedService(
                   _.GetRequiredService<IVenuesDBService>(),
                   _.GetRequiredService<ICategoriesDBService>(),
                   _.GetRequiredService<IVenuesDBBatchAddService>(),
                   _.GetRequiredService<IVenuesAPIService>()
              ));

            return services;
        }

        public static IServiceCollection AddQueryBuilderFactory(this IServiceCollection services, 
            IConfiguration configuration)
        {
            services
                .AddSingleton<IQueryBuilderFactory, QueryBuilderFactory>(_ =>
                    new QueryBuilderFactory());

            return services;
        }

        public static IServiceCollection AddHelpers(this IServiceCollection services)
        {
            services
                .AddScoped<IQueryBuilderFactory, QueryBuilderFactory>(_ => new QueryBuilderFactory());
            
            return services;
        }

        public static IServiceCollection AddGraphQL(this IServiceCollection services)
        {
            services
              .AddGraphQLServer()
              .AddProjections()
              .AddType<Query>()
              .AddType<IVenueModel>()
              .AddType<VenueModel>()
              .AddType<Venue>()
              .AddType<ICategory>()
              .AddType<Category>();

            return services;
        }

        public static IServiceCollection AddAPIServices(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services
               .AddSingleton<IFlurlClientFactory, PerBaseUrlFlurlClientFactory>();

            services
               .AddScoped<IVenuesAPIService, VenuesAPIService>(_ =>
                   new VenuesAPIService(_.GetRequiredService<IFlurlClientFactory>(),
                    configuration.GetValue<string>(Constants.DefaultVenuesEndpointConfigKey)));

            return services;
        }
    } 
}