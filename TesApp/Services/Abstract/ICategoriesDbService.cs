﻿using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public interface ICategoriesDBService:IVenuesDBBaseService 
    {
        Task<ICategory[]> GetCategories(CancellationToken cancellationToken);
    }
}
