﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public interface IVenuesDBBatchAddService : IVenuesDBBaseService
    {
        public Task<int> BatchAddAsync(IEnumerable<IVenueModel> venues);
    }
}
