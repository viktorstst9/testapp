﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public interface IVenuesAPIService
    {
        public Task<IEnumerable<IVenueModel>> FetchVenuesByCategoryAsync(
              string category,
              DateTime? after,
              CancellationToken cancellationToken);
    }
}