﻿using HotChocolate.Resolvers;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public interface IVenuesDBService
    {
         public Task<IVenueModel[]> GetVenuesByCategoryAsync(
         IResolverContext context,
         string category,
         long afterId,
         long before,
         int amount,
         CancellationToken cancellationToken);
    }
}
