﻿using Flurl.Http;
using Flurl.Http.Configuration;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TesApp.Services
{
    public class VenuesAPIBaseService
    {
        readonly IFlurlClient _flurlClient;

        //The base url is passed here because Flur handles HttpCLient life cicle internaly in singleton-ish way
        protected VenuesAPIBaseService(IFlurlClientFactory flurlClientFac, string baseUrl)
        {
            _flurlClient = flurlClientFac.Get(baseUrl);
        }

        protected Task<ResultType> GetAsync<ResultType>(
            IEnumerable<string> pathSegments,
            Dictionary<string, string> parameters,
            CancellationToken cancellationToken) =>
                _flurlClient.Request()
                 .WithAutoRedirect(true)
                .OnRedirect(c => c.Redirect.Follow = true)
                .AppendPathSegments(pathSegments)
                .SetQueryParams(parameters)
                .GetJsonAsync<ResultType>(cancellationToken);
    }
}
