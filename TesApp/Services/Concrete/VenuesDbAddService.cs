﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public class VenuesDBAddService : IVenuesDBBatchAddService
    {
        VenuesDBContext _venuesDBContext;

        private static class Constants
        {
            public const string AnyCategoryValue = "Any";
        }

        public VenuesDBAddService(IDbContextFactory<VenuesDBContext> contextFactory)
        {
            _venuesDBContext = contextFactory.CreateDbContext();
        }

        /// <summary>
        /// Batch add venueModels
        /// </summary>
        /// <param name="venueModels"></param>
        /// <returns></returns>
        public async Task<int> BatchAddAsync(IEnumerable<IVenueModel> venueModels)
        {
            if (venueModels.Count() == 0)
                return 0;

            _venuesDBContext.AddRange(venueModels.Select(venueModel => ConvertToVenue(venueModel)));

            int result = 0 ;
            try
            {
                result = await _venuesDBContext.SaveChangesAsync();
            }

            ///TODO: Find out how to suppress or remove.
            ///For speed, group adding relies on the IGNORE_DUP_KEY option on the legacy venue id.
            ///However, EF Core uses an optimistic concurrency approach and throws an exception
            ///when the expected and actual number of rows processed differs.

            catch (DbUpdateConcurrencyException)
            {
               
            }
            return result;
        }

        //TODO some injectalbe builder
        private Venue ConvertToVenue(IVenueModel venueModel) => //TODO some injectalbe builder 
          new Venue()
          {
              Id = venueModel.Id,
              Category = venueModel.Category,
              Lat = venueModel.Lat,
              Lon = venueModel.Lon,
              Name = venueModel.Name,
              Created_On = venueModel.Created_On,
              Geolocation_degrees = venueModel.Geolocation_degrees
          };
    }
}
