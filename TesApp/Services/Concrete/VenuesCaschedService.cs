﻿using HotChocolate.Resolvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public class VenuesCaschedService : IVenuesCachedService, IDisposable
    {
        private readonly IVenuesDBService _venuesDBservice;
        private readonly ICategoriesDBService _categoriesDBRepository;
        private readonly IVenuesDBBatchAddService _venuesDBAddService;
        private readonly IVenuesAPIService _venuesAPIService;
        private Task<int> _updateTask;

        public VenuesCaschedService(
            IVenuesDBService venuesDbService,
            ICategoriesDBService categoriesDbRepository,
            IVenuesDBBatchAddService venuesAddDbRepository,
            IVenuesAPIService venuesApiService)
        {
            _venuesDBservice = venuesDbService;
            _categoriesDBRepository = categoriesDbRepository;
            _venuesDBAddService = venuesAddDbRepository;
            _venuesAPIService = venuesApiService;
        }

        /// <summary>
        /// Gets venues by category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="afterId">Id after which the veneus will be retrieved</param>
        /// <param name="beforeId">Id before which the veneus will be retrieved</param>
        /// <param name="amount">The amount of venues which  will be retrieved</param>
        /// <param name="venuesDBContext">DB Context</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns></returns>
        public async Task<IVenueModel[]> GetVenuesByCategoryAsync(
            IResolverContext resolverContext,
            string category,
            long after,
            long before,
            int amount,
            CancellationToken cancellationToken)
        {
            List<IVenueModel> result = new List<IVenueModel>();
           
            var cachedVenues = 
                await _venuesDBservice?.GetVenuesByCategoryAsync(
                    resolverContext,
                    category,
                    after,
                    before,
                    amount,
                    cancellationToken);

            if (cachedVenues != null)
                result.AddRange(cachedVenues);

            //If the requested amount exceeds the received amount, then all newly created items after the
            //most recently created (by api legacy creation date) element in the local cache are read from the API
            if (MoreVenuesIsNeeded())
            {
                var newVenues = await GetNewVenues(cachedVenues, category, cancellationToken);

                if (newVenues != null)
                {
                    result.AddRange(newVenues.Take(amount).OrderBy(venue => venue.Id));

                    _updateTask = _venuesDBAddService?.BatchAddAsync(newVenues);
                }
            }

            //Is 'Next Page call' and retrived amaount is less than requested
            bool MoreVenuesIsNeeded() => after > 0 && cachedVenues.Count() < amount;

            return result.ToArray();
        }

        /// <summary>
        /// Gets only the elements that are absent from the local cache
        /// using the last creation date in the cached items as the starting date for the API call
        /// </summary>
        /// <param name="cachedVenues"></param>
        /// <param name="category"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task<IVenueModel[]> GetNewVenues(
            IVenueModel[] cachedVenues, 
            string category, 
            CancellationToken cancellationToken )
        {
            var nonCachedVenues = await _venuesAPIService?.FetchVenuesByCategoryAsync(
                                 category,
                                 GetMaxCreationDate(cachedVenues),
                                 cancellationToken);
            if (nonCachedVenues?.Count() == 0)
                return null;

            return nonCachedVenues.Where(nonCached => !cachedVenues.Any(cached => nonCached.Id == cached.Id))
                .OrderBy(venues=>venues.Id)
                .ToArray();
        }

        public void Dispose()
        {
            _updateTask?.Wait();
        }

        public Task<ICategory[]> GetCategories(CancellationToken cancellationToken)
        {
            return _categoriesDBRepository?.GetCategories(cancellationToken);
        }

        /// <summary>
        /// Gets the most recent legacy venue creation date from a venues collection
        /// </summary>
        /// <param name="venues"></param>
        /// <returns></returns>
        private static DateTime? GetMaxCreationDate( IEnumerable<IVenueModel> venues)
        {
            if (venues == null || venues.Count() == 0)
                return null;

            DateTime result;

            long timeStamp = venues.Max(venue => venue.Created_On);

            try
            {
                result = DateTimeOffset.FromUnixTimeSeconds(timeStamp).DateTime; 
            }
            catch(ArgumentOutOfRangeException)
            {
                return null;
            }

            return result;
        }
    }
}
