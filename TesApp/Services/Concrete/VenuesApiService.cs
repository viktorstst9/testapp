﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TesApp.Data;
using Flurl.Http.Configuration;
using System.Threading;
using System;
using System.Collections;
using System.Linq;

namespace TesApp.Services
{
    public class VenuesAPIService : VenuesAPIBaseService, IVenuesAPIService
    {
        private static class Endpoints
        {
            public const string Venues = "venues";
        }

        private static class ParameterNames
        {
            public const string After = "after";
            public const string Category = "category";

        }

        private static Dictionary<string, ParameterInfo> parameterInfos = new Dictionary<string, ParameterInfo>
        {
            {ParameterNames.After, new ParameterInfo(format: "yyyy-MM-dd", null) },
            {ParameterNames.Category, new ParameterInfo(format:"","Any", null) }
        };


        private Dictionary<string, string> GetParameters(params KeyValuePair<string,object>[] parameters)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var parameter in parameters)
            {
                if (!parameterInfos.TryGetValue(parameter.Key, out ParameterInfo parameterInfo))
                    continue;

                if (parameterInfo.IsEmpty(parameter.Value))
                    continue;

                if (parameter.Value is IFormattable formattable)
                    result.Add(parameter.Key, formattable.ToString(parameterInfo.Format, null));
                else
                    result.Add(parameter.Key, parameter.Value.ToString());
            }
            return result;
        }

        private class ParameterInfo
        {
            public ParameterInfo(string format = "", params object[] emptyValues)
            {
                emptyValuesContainer = emptyValues;
                Format = format;
            }
            public bool IsEmpty(object value) => value==null 
                || (emptyValuesContainer == null ? false : emptyValuesContainer.Contains(value));
            
            public string Name { get;}
           
            public string Format { get; }
            private object[] emptyValuesContainer { get; }
        }

        public VenuesAPIService(IFlurlClientFactory flurlClientFac, string baseUrl) 
            : base(flurlClientFac, baseUrl)
        {

        }
      
        public Task<IEnumerable<IVenueModel>> FetchVenuesByCategoryAsync(
           string category,
           DateTime? after,
           CancellationToken cancellationToken)
        {
            Task<IEnumerable<IVenueModel>> result = Task.Run( async () => 
            {
               var  response = await GetAsync<IVenuesResponse>(
                   new[] { Endpoints.Venues },
                      GetParameters(
                          new KeyValuePair<string, object>(ParameterNames.Category, category),
                          new KeyValuePair<string, object>(ParameterNames.After, after)
                          ),
                   cancellationToken);

                // TODO: Error handling 
                return response?.Venues ;
            });

            return result;
        }
    }
}
