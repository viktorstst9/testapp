﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;

namespace TesApp.Services
{
    public class CategoriesDBService : ICategoriesDBService
    {
        private static class Constants
        {
            public const string AnyCategoryValue = "Any";
        }

        private VenuesDBContext _venuesDBContext;

        public CategoriesDBService(IDbContextFactory<VenuesDBContext> dbContextFactory)
        {
            _venuesDBContext = dbContextFactory.CreateDbContext();
        }

        public Task<ICategory[]> GetCategories(
             CancellationToken cancellationToken) => 
                GetCategories(
                    _venuesDBContext,
                    cancellationToken);

        private Task<ICategory[]> GetCategories(
            VenuesDBContext venuesDBContext,
            CancellationToken cancellationToken) =>
            venuesDBContext.Set<Category>()
            .Select(category => category as ICategory)
            .ToArrayAsync();
    }
}
