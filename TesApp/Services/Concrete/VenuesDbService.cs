﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TesApp.Data;
using TesApp.ExpressionUtils;
using HotChocolate.Resolvers;

namespace TesApp.Services
{
    public class VenuesDBService : IVenuesDBService
    {
        private static class Constants
        {
            public const string AnyCategoryValue = "Any";
        }

        IQueryBuilderFactory _queryBuilderFactory;
        VenuesDBContext _venuesDBContext;
        IModelBaseFactory _modelBaseFactory;

        public VenuesDBService(
            IDbContextFactory<VenuesDBContext>  dbContextFactory,
            IQueryBuilderFactory queryBuilderFactory,
            IModelBaseFactory modelBaseFactory )
        {
            _queryBuilderFactory = queryBuilderFactory;
            _venuesDBContext = dbContextFactory.CreateDbContext();
            _modelBaseFactory = modelBaseFactory;
        }


        /// <summary>
        /// Gets venues by category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="afterId">Id after which the veneus will be retrieved</param>
        /// <param name="beforeId">Id before which the veneus will be retrieved</param>
        /// <param name="amount">The amount of venues which  will be retrieved</param>
        /// <param name="venuesDBContext">DB Context</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns></returns>
        public Task<IVenueModel[]> GetVenuesByCategoryAsync(
           IResolverContext context,
           string category,
           long afterId,
           long beforeId,
           int amount,
           CancellationToken cancellationToken) =>
               GetVenuesByCategoryAsync(
                _venuesDBContext,
                _queryBuilderFactory.Create(),
                context,
                (IVenueModelFactory) _modelBaseFactory.Create<IVenueModelFactory>(),
                category,
                afterId,
                beforeId,
                amount,
                cancellationToken);
      
        /// <summary>
        /// Gets venues by category
        /// </summary>
        /// <param name="category"></param>
        /// <param name="afterId">Id after which the veneus will be retrieved</param>
        /// <param name="beforeId">Id before which the veneus will be retrieved</param>
        /// <param name="amount">The amount of venues which  will be retrieved</param>
        /// <param name="venuesDBContext">DB Context</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns></returns>
        private Task<IVenueModel[]> GetVenuesByCategoryAsync(
        VenuesDBContext venuesDBContext,
        IQueryBuilder queryBuilder,
        IResolverContext context,
        IVenueModelFactory venueModelFactory,
        string category,
        long afterId,
        long beforeId,
        int amount,
        CancellationToken cancellationToken)
        {
            var where = queryBuilder.True<Venue>();
           
            if (!IsAnyCategory(category))
                where = queryBuilder.And(where, (venue => venue.Category == category));
             
            where = IsBackPageCall() 
                ? queryBuilder.And(where, venue => venue.Id < beforeId)
                : queryBuilder.And(where, venue => venue.Id > afterId);
            
            var venuesQuery= queryBuilder.OrderByDynamic( 
                                                            venue => venue.Id,
                                                            IsBackPageCall(), 
                                                            false,
                                                            venuesDBContext.Set<Venue>()
                                            .Where(where))
                              .Take(amount);
            
            venuesQuery = queryBuilder.OrderByDynamic( venue => venue.Id, false, !IsBackPageCall(), venuesQuery);

            return venuesQuery
                    //TODO Remove
                    //.Project<IVenueModel>(context) 
                    .Select(venue => venueModelFactory.Create(venue))
                    .AsNoTracking()
                    .ToArrayAsync() ;
          
            bool IsBackPageCall() => beforeId > 0;

            bool IsAnyCategory(string category) => 
                !string.IsNullOrEmpty(category) 
                && category != Constants.AnyCategoryValue;
        }
    }
}
