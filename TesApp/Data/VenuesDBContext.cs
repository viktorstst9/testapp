﻿using Microsoft.EntityFrameworkCore;

namespace TesApp.Data
{
    public class VenuesDBContext : DbContext
    {
        public VenuesDBContext(DbContextOptions<VenuesDBContext> options ) 
            : base(options)
        {
          if( Database.EnsureCreated())
            {
                Database.ExecuteSqlRaw("ALTER INDEX[IX_Venues_Id] ON[dbo].[Venues] REBUILD  WITH(IGNORE_DUP_KEY = ON)");
            }
        }

        public DbSet<Venue> Venues { get; set; }
        public DbSet<Category>  Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venue>(entity =>
            {
                entity.HasKey(venue => venue.VenueId);
                entity
                    .Property(venue => venue.VenueId)
                    .ValueGeneratedOnAdd();

                entity
                    .Property(venue => venue.Category)
                    .HasMaxLength(50);
                entity.HasIndex(venue => venue.Category);

                entity.HasIndex(venue => venue.Id)
                        .IsUnique();
            });

            modelBuilder.Entity<Category>(entity =>
            {
            entity.HasKey(category => category.CategoryId);
            entity
                .Property(category => category.CategoryId)
                .ValueGeneratedOnAdd();

            entity
                .Property(category => category.Name)
                .HasMaxLength(50);

            entity.HasData(
                     new { CategoryId = 1L, Name = "atm" },
                     new { CategoryId = 2L, Name = "cafe" },
                     new { CategoryId = 3L, Name = "grocery" },
                     new { CategoryId = 4L, Name = "default" },
                     new { CategoryId = 5L, Name = "shopping" },
                     new { CategoryId = 6L, Name = "lodging" },
                     new { CategoryId = 7L, Name = "nightlife" },
                     new { CategoryId = 8L, Name = "attraction" },
                     new { CategoryId = 9L, Name = "food" },
                     new { CategoryId = 10L, Name = "transport" },
                     new { CategoryId = 11L, Name = "sports" },
                     new { CategoryId = 12L, Name = "trezor retaile" },
                     new { CategoryId = 13L, Name = "Travel Agency" },
                     new { CategoryId = 14L, Name = "ATM" },
                     new { CategoryId = 15L, Name = "Grocery" },
                     new { CategoryId = 16L, Name = "Educational Business" },
                     new { CategoryId = 17L, Name = "services" },
                     new { CategoryId = 18L, Name = "retail" }
                    );
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
