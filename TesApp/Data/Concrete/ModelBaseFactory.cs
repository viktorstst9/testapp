﻿using System;

namespace TesApp.Data
{
    public class ModelBaseFactory:IModelBaseFactory
    {
        IServiceProvider _provider;

        public ModelBaseFactory(IServiceProvider provider)
        {
            _provider = provider;
        }

        public IModelFactory Create<ModelFactory>()
        {
           return (IModelFactory) _provider.GetService(typeof(ModelFactory));
        }
    }
}
