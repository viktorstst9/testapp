﻿namespace TesApp.Data
{
    public class VenueModelFactory : IVenueModelFactory
    {
        public IVenueModel Create()
        {
            return new VenueModel();
        }

        public IVenueModel Create(Venue venueEnitiy)
        {
            return new VenueModel()
            {
                Id = venueEnitiy.Id,
                Category = venueEnitiy.Category,
                Name = venueEnitiy.Name,
                Lat = venueEnitiy.Lat,
                Lon = venueEnitiy.Lon,
                Created_On = venueEnitiy.Created_On,
                Geolocation_degrees = venueEnitiy.Geolocation_degrees
            };
        }
    }
}
