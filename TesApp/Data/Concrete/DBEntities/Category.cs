﻿namespace TesApp.Data
{
    public class Category : ICategory
    {
        public long CategoryId { get; set; }

        public string Name { get; set; }
    }
}
