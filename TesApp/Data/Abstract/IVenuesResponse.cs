﻿using System.Collections.Generic;
namespace TesApp.Data
{
    public interface IVenuesResponse
    {
        public IEnumerable<IVenueModel> Venues { get; set; }
    }
}
