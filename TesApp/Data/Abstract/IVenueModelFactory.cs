﻿namespace TesApp.Data
{
    public interface IVenueModelFactory: IModelFactory
    {
        public IVenueModel Create();
        public IVenueModel Create(Venue venueEnitiy);
    }
}