﻿namespace TesApp.Data
{
    public interface ICategory:IModel
    {
        long CategoryId { get; set; }
        string Name { get; set; }
    }
}