﻿namespace TesApp.Data
{
    public interface IModelBaseFactory
    {
        public IModelFactory Create<ModelFactory>();
    }
}