using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestAppClient.Consumers;

namespace TestAppClient
{
    public partial class VenuesForm : Form
    {
        private static class Constants
        {
            public const int DefaultItemsPerPage = 10;
        }
        
        IVenuesConsumer _venuesConsumer;

        private int _pageNum = 0;

        private IEnumerable<Edge> _edges;

        private bool categoriesIsLoaded = false;

        public VenuesForm(IVenuesConsumer venuesConsumer )
        {
            _venuesConsumer = venuesConsumer;

            InitializeComponent();

            PrepareControls();
        
            FillCategoiesCombo();
        }
        
        private void PrepareControls()
        {
            dgvVenues.AutoGenerateColumns = false;
            cmbCategories.DisplayMember = "Name";
            cmbCategories.ValueMember = "CategoryId";
           
            numItemsPerPage.Value = Constants.DefaultItemsPerPage;
        }

        private async void FillCategoiesCombo()
        {
            try
            {
                SetLoadingState(true);
                cmbCategories.SelectedIndexChanged -= cmbCategories_SelectedIndexChanged;
                var categories = await FetchCategories();

                cmbCategories.DataSource = categories;

                cmbCategories.SelectedIndexChanged += cmbCategories_SelectedIndexChanged;
                categoriesIsLoaded = true; 
                timer.Stop();

                //Find out when the cmbCategories.DataSourceis reday and put it there
                if (categoriesIsLoaded)
                    NextPage(categories.Select(c => c.Name).DefaultIfEmpty("atm").FirstOrDefault(), SelectedItemsPerPage);
            }
            catch(Exception ex)
            {
                if (timer.Enabled)
                    return;

                MessageBox.Show(ex.Message);
                categoriesIsLoaded = false;
                timer.Start();
            }
            finally
            {
                SetLoadingState(false);
            }
        }

        private Task<IEnumerable<Category>> FetchCategories() 
        {
            try
            {
                SetLoadingState(true);
                return _venuesConsumer.GetCategories();
            }
            finally
            {
                SetLoadingState(false);
            }
        }

        private async Task<IEnumerable<Edge>> FetchVenues(string category, long beforeId, long afterId, int amount)
        {
            SetLoadingState(true);

            IEnumerable<Edge> result;
            try
            {
                result = await _venuesConsumer.FetchVenuesByCategory(category, beforeId, afterId, amount);
            }
            finally
            {
                SetLoadingState(false);
            }

            return result;
        }

        private void SetLoadingState(bool isLoading)
        {
            Cursor = isLoading ? Cursors.WaitCursor : Cursors.Default;
            btnNext.Enabled = !isLoading;
            btnPrev.Enabled = !isLoading;
            cmbCategories.Enabled = !isLoading;
            numItemsPerPage.Enabled = !isLoading;
        }
        
        private string SelectedCategory => (cmbCategories.SelectedItem as Category)?.Name;

        private int SelectedItemsPerPage => (int)numItemsPerPage.Value;

        private void NextPage(string category, int itemsPerPage)
        {
            _ = GetPage(category, GetLastCursor(), 0, itemsPerPage);
           
            _pageNum++;
        }

        private void PrevPage(string category, int itemsPerPage)
        {
            if (_pageNum == 1)
                return;

                _ = GetPage(category, 0, GetFirstCursor(), itemsPerPage);

            _pageNum--;
        }

        private async Task GetPage(string category, long beforeId, long afterId, int itemsPerPage)
        {
            try
            {
                _edges = await FetchVenues(category, beforeId, afterId, itemsPerPage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
            FillGrid(_edges.Select(edge => ConvertToVenueModel(edge.Node)));
        }

        private void FillGrid(IEnumerable<VenueModel> venues) =>
            dgvVenues.DataSource = venues.ToList();

        private long GetFirstCursor() 
        {
            if (_edges == null || _edges.Count() == 0)
                return 0;

           return _edges.Min(edge => edge.Cursor);
        }

        private long GetLastCursor()
        {
            if (_edges == null || _edges.Count() == 0)
                return 0;

            return _edges.Max(edge => edge.Cursor);
        }

        private void cmbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
             NextPage(
                    SelectedCategory,
                    SelectedItemsPerPage);

        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            NextPage(
                  SelectedCategory,
                  SelectedItemsPerPage);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            PrevPage(
                 SelectedCategory,
                 SelectedItemsPerPage);
        }

        private VenueModel ConvertToVenueModel(Node node) => new VenueModel(node.Id)
        {
            Name = node.Name,
            Lat = node.Lat,
            Lon = node.Lon,
            Category = node.Category,
            Created_On = DateTimeOffset.FromUnixTimeSeconds(node.Created_On).DateTime,
            Geolocation_degrees = node.Geolocation_degrees,
        };

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!categoriesIsLoaded)
                FillCategoiesCombo();
        }
    }
}
