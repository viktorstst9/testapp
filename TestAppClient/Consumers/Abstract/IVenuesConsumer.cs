﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestAppClient.Consumers
{
    public interface IVenuesConsumer
    {
        public Task<IEnumerable<Category>> GetCategories();
        public Task<IEnumerable<Edge>> FetchVenuesByCategory(string category, long afterId,long before,  int amount);
    }
}
