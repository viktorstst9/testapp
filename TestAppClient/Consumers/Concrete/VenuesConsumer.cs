﻿using GraphQL;
using GraphQL.Client.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using VenuesClient.Exceptions;

namespace TestAppClient.Consumers
{
    public class VenuesConsumer: IVenuesConsumer
    {
        //TODO store queries templete somewhere else
        private readonly IGraphQLClient _client;

        public VenuesConsumer(IGraphQLClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            string queryTemplate = @"query {categories {
                                        name
                                     }
                                    }";

            var query = new GraphQLRequest
            {
                Query = queryTemplate
            };

            var response = await _client.SendQueryAsync<CategoriesResponseType>(query);

            return response.Data.Categories;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="afterId"></param>
        /// <param name="beforeId"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Edge>> FetchVenuesByCategory(
            string category,
            long afterId,
            long beforeId,
            int first)
        {

            string queryTemplate = @"
                 query ($category: String!, $afterId:String, $beforeId:String, $first:Int) {
                    venues (category: $category, after: $afterId, before: $beforeId, first: $first) {
                        edges{
                                    cursor
                                    node{
                                        id
                                        category
                                      lat
                            lon
                                      name
                            created_On
                                      geolocation_degrees
                                    }
                                }
                            }
            }";

            var query = new GraphQLRequest
            {
                Query = queryTemplate,
                Variables = new 
                {
                    category = category,
                    afterId = afterId.ToString(),
                    beforeId = beforeId.ToString(),
                    first = first
                }
            };
           
            var response = await _client.SendQueryAsync<VenuesPagingCollectionResponseType>(query);

            CheckResponse(response);

            return response.Data.Venues.Edges;
        }

        private static void CheckResponse(IGraphQLResponse response)
        {
            if(response.Errors?.Length > 0)
                throw new QueryException("Errors msg"); //TODO

            //Log
        }
    }
}
