﻿namespace TestAppClient.Consumers
{
    public class Node
    {
        public long Id { get; set; }
        public string Category { get; set; }
        public float Lat { get; set; }
        public string Lon { get; set; }
        public string Name { get; set; }
        public long Created_On { get; set; }
        public string Geolocation_degrees { get; set; }
     }
}
