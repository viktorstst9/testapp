﻿namespace TestAppClient.Consumers
{
    public class Edge
    {
        public long Cursor { get; set; }
        public Node Node {get;set;}
    }
}
