﻿using System.Collections.Generic;

namespace TestAppClient.Consumers
{
    public class CategoriesResponseType
    {
        public IEnumerable<Category> Categories { get; set; }
    }
}
