﻿namespace TestAppClient.Consumers
{
    public class Category
    {
        public long CategoryId { get; set; }

        public string Name { get; set; }
    }
}
