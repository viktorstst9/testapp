﻿using System.Collections.Generic;

namespace TestAppClient.Consumers
{
    public class Venue
    {
        public IEnumerable<Edge> Edges { get; set; }
    }
}
