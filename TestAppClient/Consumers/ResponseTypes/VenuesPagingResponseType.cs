﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAppClient.Consumers
{
    public class VenuesPagingCollectionResponseType
    {
        public Venue Venues { get; set; }
    }
}
