namespace TestAppClient
{
    using GraphQL.Client.Http;
    using GraphQL.Client.Serializer.Newtonsoft;
    using System;
    using System.Windows.Forms;
    using TestAppClient.Consumers;
    static class Program
    {
        //TODO: put in configs
        private static string GraphQLURI = "https://localhost:5001/graphql/";
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new VenuesForm(new VenuesConsumer( new GraphQLHttpClient(GraphQLURI, new NewtonsoftJsonSerializer()))));
        }
    }
}
