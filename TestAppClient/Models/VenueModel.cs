﻿using System;
using System.ComponentModel;

namespace TestAppClient.Consumers
{
    public class VenueModel
    {
        public VenueModel(long Id)
        {
            this.Id = Id;
        }
        public long Id { get; private set; }

        public string Category { get; set; }
        public float Lat { get; set; }
        public string Lon { get; set; }
        public string Name { get; set; }
        [DisplayName("Creation date")]
        public DateTime Created_On { get; set; }
        public string Geolocation_degrees { get; set; }
    }
}
