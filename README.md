# TestApp

## Description

This project provides a locally cached in MS SQL database,
automatically updated from https://coinmap.org/api/ list of venues 

## Overview 

- Backend
    Project name: Venues
	Used stuff: 
				MS SQL Database
				.NET Core application
				Entity Framework Core
				Hot Chocolate GraphQL server
		
- Frontend: 
	Project name: VenuesClient
	Used stuff: 
				WinForm, 
				GraphQL Client
		
## Details

The database consists of two tables : Categories and Venues.
Code First was used. The base will be created automatically without migration on the connection string specified in appsettings.json.
The categories are seeded when the base is created. Fetching them from the API is a relatively heavy distinct operation on ~29000 records 
that can be done from time to time but not during user iteration. I didn't find an endpoint that provides them.
Cursor-based backend paging is used, and at the moment this is done by the venue's legacy id.
Hot Chocolate has out of the box solution for paging but I tried an implementation without exposing the entity set 
to the resolver so that there could possibly be some intermediate services and models.

## Installation

there should be a MS SQL instance runing on localhost so everything can work with the default connection.


## TODSO 

- [ ] User authentication
- [ ] More loosely coupled calling of the database update (The Get function is doing to much )
- [ ] Unit tests
- [ ] Excepitons hendaling
- [ ] Reafactoring 


